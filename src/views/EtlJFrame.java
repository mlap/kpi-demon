package views;
import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.LookAndFeel;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import java.awt.Font;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import org.joda.time.DateTime;

import models.PeriodCount;

import javax.swing.border.TitledBorder;
import javax.swing.JSeparator;
import javax.swing.JToggleButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Toolkit;
import javax.swing.JButton;

public class EtlJFrame extends JFrame  implements Runnable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 286000821846657631L;
	private JPanel contentPane;
	private JTable table;
	private JToggleButton status;

    Thread hilo = new Thread( this );
    private JLabel lblTiempoRestante;
    DateTime dateTime = new DateTime(2005, 3, 26, 12, 0, 0, 0);
    private JPanel panel;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				LookAndFeel lookAndFeel = UIManager.getLookAndFeel();
				try {
					UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
				} catch (Exception e) {
					try {
						UIManager.setLookAndFeel(lookAndFeel);
					} catch (UnsupportedLookAndFeelException e1) {
						JOptionPane.showMessageDialog(
								   null,
								   "Error: "+e1.getMessage().toString());
					}
				} 
				
				try {
					EtlJFrame frame = new EtlJFrame();
					frame.setVisible(true);
				} catch (Exception e) {
					JOptionPane.showMessageDialog(
							   null,
							   "Error: "+e.getMessage().toString());
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public EtlJFrame() {
		setIconImage(Toolkit.getDefaultToolkit().getImage(EtlJFrame.class.getResource("/img/data-storage.png")));
		setTitle("ETL - Extracci\u00F3n, transformaci\u00F3n y carga");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 703, 418);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		
		panel = new JPanel();
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addComponent(panel, Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, 574, Short.MAX_VALUE)
		);
		gl_contentPane.setVerticalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addComponent(panel, GroupLayout.DEFAULT_SIZE, 369, Short.MAX_VALUE)
		);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBorder(new TitledBorder(null, "Registro histórico de operaciones de extracción, transformación y carga", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		
		JPanel panel_2 = new JPanel();
		
		JPanel panel_3 = new JPanel();
		GroupLayout gl_panel = new GroupLayout(panel);
		gl_panel.setHorizontalGroup(
			gl_panel.createParallelGroup(Alignment.TRAILING)
				.addGroup(gl_panel.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
						.addComponent(panel_2, GroupLayout.DEFAULT_SIZE, 554, Short.MAX_VALUE)
						.addComponent(panel_1, GroupLayout.DEFAULT_SIZE, 554, Short.MAX_VALUE)
						.addGroup(gl_panel.createSequentialGroup()
							.addComponent(panel_3, GroupLayout.DEFAULT_SIZE, 552, Short.MAX_VALUE)
							.addGap(2)))
					.addContainerGap())
		);
		gl_panel.setVerticalGroup(
			gl_panel.createParallelGroup(Alignment.TRAILING)
				.addGroup(gl_panel.createSequentialGroup()
					.addContainerGap()
					.addComponent(panel_2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
					.addGap(16)
					.addComponent(panel_1, GroupLayout.DEFAULT_SIZE, 205, Short.MAX_VALUE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(panel_3, GroupLayout.PREFERRED_SIZE, 64, GroupLayout.PREFERRED_SIZE)
					.addContainerGap())
		);
		
		JLabel label = new JLabel("\u00DAltima actualizaci\u00F3n: 2018-09-01, Hora: 12:30 pm.");
		label.setHorizontalAlignment(SwingConstants.CENTER);
		
		JLabel lblPrximaActualizacin = new JLabel("Pr\u00F3xima actualizaci\u00F3n: 2018-09-01, Hora: 12:30 pm.");
		lblPrximaActualizacin.setHorizontalAlignment(SwingConstants.CENTER);
		
		lblTiempoRestante = new JLabel("Tiempo restante: 01 D\u00EDas, 2 horas y  30 minutos con 60 segundos.");
		lblTiempoRestante.setHorizontalAlignment(SwingConstants.CENTER);
		GroupLayout gl_panel_3 = new GroupLayout(panel_3);
		gl_panel_3.setHorizontalGroup(
			gl_panel_3.createParallelGroup(Alignment.LEADING)
				.addGroup(Alignment.TRAILING, gl_panel_3.createSequentialGroup()
					.addGap(1)
					.addGroup(gl_panel_3.createParallelGroup(Alignment.LEADING)
						.addComponent(lblTiempoRestante, GroupLayout.DEFAULT_SIZE, 552, Short.MAX_VALUE)
						.addComponent(lblPrximaActualizacin, GroupLayout.DEFAULT_SIZE, 552, Short.MAX_VALUE)
						.addComponent(label, Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, 552, Short.MAX_VALUE))
					.addContainerGap())
		);
		gl_panel_3.setVerticalGroup(
			gl_panel_3.createParallelGroup(Alignment.TRAILING)
				.addGroup(Alignment.LEADING, gl_panel_3.createSequentialGroup()
					.addComponent(label)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblPrximaActualizacin)
					.addGap(10)
					.addComponent(lblTiempoRestante)
					.addContainerGap(1, Short.MAX_VALUE))
		);
		panel_3.setLayout(gl_panel_3);
		
		JLabel lblNewLabel = new JLabel("Migraciones - ETL");
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 20));
		
		JSeparator separator = new JSeparator();
		separator.setOrientation(SwingConstants.VERTICAL);
		
		JLabel lblEstadoDelAnlisis = new JLabel("Estado del an\u00E1lisis:");
		lblEstadoDelAnlisis.setFont(new Font("Tahoma", Font.PLAIN, 15));
		
		status = new JToggleButton("En ejecuci\u00F3n");
		status.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (status.isSelected()) {
					status.setText("En ejecución");
					JOptionPane.showMessageDialog(
							   panel,
							   "Se ha reanudado la ejecución del ETL");
				}
				else {
					status.setText("ETL en pausa");
					JOptionPane.showMessageDialog(
							   panel,
							   "Se ha detenido la ejecución del ETL");
				}
			}
		});
		status.setSelected(true);
		
		JButton btnNewButton = new JButton("Modificar periodicidad del ETL");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				EtlPeriodicityJDialog etlPeriodicityJDialog=new EtlPeriodicityJDialog();
				etlPeriodicityJDialog.setLocationRelativeTo(panel);
				etlPeriodicityJDialog.setModal(true);
				etlPeriodicityJDialog.setVisible(true);
			}
		});
		GroupLayout gl_panel_2 = new GroupLayout(panel_2);
		gl_panel_2.setHorizontalGroup(
			gl_panel_2.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_2.createSequentialGroup()
					.addGroup(gl_panel_2.createParallelGroup(Alignment.LEADING)
						.addComponent(lblNewLabel, GroupLayout.PREFERRED_SIZE, 186, GroupLayout.PREFERRED_SIZE)
						.addGroup(gl_panel_2.createSequentialGroup()
							.addComponent(lblEstadoDelAnlisis)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(status, GroupLayout.PREFERRED_SIZE, 107, GroupLayout.PREFERRED_SIZE)))
					.addGap(32)
					.addComponent(separator, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(btnNewButton, GroupLayout.DEFAULT_SIZE, 250, Short.MAX_VALUE)
					.addContainerGap())
		);
		gl_panel_2.setVerticalGroup(
			gl_panel_2.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_2.createSequentialGroup()
					.addComponent(lblNewLabel)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_panel_2.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblEstadoDelAnlisis, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE)
						.addComponent(status, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE))
					.addGap(0, 0, Short.MAX_VALUE))
				.addComponent(separator, GroupLayout.DEFAULT_SIZE, 58, Short.MAX_VALUE)
				.addGroup(gl_panel_2.createSequentialGroup()
					.addContainerGap()
					.addComponent(btnNewButton, GroupLayout.DEFAULT_SIZE, 36, Short.MAX_VALUE)
					.addContainerGap())
		);
		panel_2.setLayout(gl_panel_2);
		panel_1.setLayout(new BorderLayout(0, 0));
		
		JScrollPane scrollPane = new JScrollPane();
		panel_1.add(scrollPane, BorderLayout.CENTER);
		
		table = new JTable();
		table.setModel(new DefaultTableModel(
			new Object[][] {
				{"138.231.223.123", "14.139.52.17", "2018-07-09", "11:12 pm."},
				{"138.231.223.123", "14.139.52.17", "2018-07-08", "12:09 pm."},
				{"138.231.223.123", "14.139.52.17", "2018-07-07", "08:22 pm."},
				{"138.231.223.123", "14.139.52.17", "2018-07-07", "04:31 pm."},
				{"138.231.223.123", "14.139.52.17", "2018-07-06", "11:40 pm."},
				{"138.231.223.123", "14.139.52.17", "2018-07-06", "12:05 pm."},
				{"138.231.223.123", "14.139.52.17", "2018-07-06", "03:00 pm."},
				{"138.231.223.123", "14.139.52.17", "2018-07-06", "07:57 pm."},
				{"64.255.63.35", "14.139.52.17", "2018-07-06", "12:14 am."},
				{"64.255.63.35", "14.139.52.17", "2018-07-05", "05:48 pm."},
				{"64.255.63.35", "14.139.52.17", "2018-07-05", "12:59 pm."},
				{"64.255.63.35", "14.139.52.17", "2018-07-05", "08:55 am."},
				{"64.255.63.35", "14.139.52.17", "2018-07-05", "01:23 am."},
			},
			new String[] {
				"IP origen", "IP destino", "fecha de ejecución", "Hora"
			}
		));
		scrollPane.setViewportView(table);
		panel.setLayout(gl_panel);
		contentPane.setLayout(gl_contentPane);

        hilo.start();
	}
	public void run() {
		PeriodCount periodCount = new PeriodCount(3, 0, 0, 3);
        try {
	        while (true) {
				Thread.sleep( 1000 );
				if (status.isSelected()) {
		    		lblTiempoRestante.setText("Tiempo restante: "+periodCount.getDays()+" D\\u00EDas, "+periodCount.getHours()+" horas y  "+periodCount.getMinutes()+" minutos con "+periodCount.getSeconds()+" segundos");
		    		periodCount.addSeconds(-1);
				}
	        }
		} catch (InterruptedException e) {
			JOptionPane.showMessageDialog(panel, "Error: "+e.getMessage().toString());
			lblTiempoRestante.setText("Tiempo restante: 0 D\u00EDas, 0 horas y  0 minutos con 0 segundos.");
		}
	}
}
